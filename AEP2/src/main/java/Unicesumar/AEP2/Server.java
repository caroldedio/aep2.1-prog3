package Unicesumar.AEP2;

import java.io.File;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
	private static final int port = 9092;
	
	public static void main(String[] args) {
		Server s = new Server();
		s.start();
	}
	
	public void start() {
				
		try {
			Scanner fromClient = new Scanner(System.in);
			String comando = "";
			ServerSocket server = null; 
			Socket client = null;
			do {
				comando = fromClient.nextLine();
				if(comando.equals("conectar")) {
					server = new ServerSocket(port);
					client = server.accept();
					PrintWriter toClient = new PrintWriter(client.getOutputStream());
					toClient.println("Server conectado");
					toClient.flush();
				} else if(comando.equals("desconectar")){
					server.close();
					PrintWriter toClient = new PrintWriter(client.getOutputStream());
					toClient.println("Server desconectado");
					toClient.flush();
				} else if(comando.equals("listar")) {
					File raiz = new File("D:/");
					percorre(raiz,"");					
				} else if(comando.equals("remover")) {
				}
			} while (!comando.equals("sair"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void percorre(File e, String i) {
		System.out.println(i + e.getAbsolutePath());
		if(e.isDirectory()) {
			for(File arquivo: e.listFiles()) {}
				percorre(arquivo, i + "");
		}
	}
}
