package Unicesumar.AEP2;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) {
		Client c = new Client();
		c.start();
	}
	
	private void start() {
		try {
			Scanner console = new Scanner(System.in);
			Socket server =new Socket("localhost", 9092);
			PrintWriter toServer = new PrintWriter(server.getOutputStream());
			Scanner fromServer = new Scanner(server.getInputStream());
			do {
				System.out.println("Aguardando comando:");
				String comando = console.nextLine();
				toServer.println(comando);
				toServer.flush();
				String resposta = fromServer.nextLine();
				System.out.println("resposta" + resposta);
			} while(true);

		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
